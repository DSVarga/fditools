echo on
% Welcome to FDITOOLS collection of Fault Detection and Isolation Tools.
% The primary source for the FDITOOLS repository is
%
%     https://bitbucket.org/DSVarga/fditools/downloads
% 
% The FDITOOLS collection comprises the M-files in this directory. 
%
% To run FDITOOLS, the Descriptor System Tools (DSTOOLS) must be 
% additionally installed. The primary source for the DSTOOLS repository is
%
%     https://bitbucket.org/DSVarga/dstools/downloads
% 
% For the list of M-files in FDITOOLS type
%     help fditools
echo off
