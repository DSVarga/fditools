# **FDITOOLS - Fault Detection and Isolation Tools for MATLAB**  #


## About 

`FDITOOLS` is a collection of MATLAB functions for the analysis and solution 
of fault detection problems. The functions of this collection relies on 
the Control System Toolbox and the Descriptor System Tools (`DSTOOLS`). 

The implemented functions are based on the computational procedures described in Chapters 5, 6 and 7 of the book:

Andreas Varga, "[Solving Fault Diagnosis Problems, Linear Synthesis Techniques](http://www.springer.com/us/book/9783319515588)", vol. 84 of Studies in Systems, Decision and Control, Springer International Publishing, xxviii+394, 2017.

This book describes the mathematical background of solving synthesis problems of fault detection and model detection filters and gives detailed descriptions of the underlying synthesis procedures. 

The current release of `FDITOOLS` is version V1.0.5, dated July 7, 2019.

The User's Guide of the version V1.0 of the `FDITOOLS` collection is provided in the file `fditoolsdoc.pdf`. Additionally, the M-files of the functions are self-documenting and a detailed documentation of each function can be obtained online by typing help with the corresponding M-file name. 



## Requirements

The codes have been developed under MATLAB 2015b and have been tested with MATLAB 2016a through 2019a. 
To use the functions, the Control System Toolbox and the `DSTOOLS` collection (Version V0.71 or later) must be installed in MATLAB running under 64-bit Windows 7, 8, 8.1 or 10. 

## License

* See `license.txt` for licensing information.

* Please cite `FDITOOLS` as "A. Varga. FDITOOLS - The Fault Detection and Isolation Tools for MATLAB. https://sites.google.com/site/andreasvargacontact/home/software/fditools, 2018."

* Please cite the documentation of `FDITOOLS` as "A. Varga. Fault Detection and Isolation Tools (FDITOOLS) User's Guide, ArXiv eprint [arXiv:1703.08480](https://arxiv.org/abs/1703.08480), November 2018."

